import { useState } from 'react';
import { API_KEY, API_LIMIT } from '../../constants/constants';
import makeRequest from '../../utils/request';
import RequestConfig from '../../utils/request.config';

import './Search.scss';

function Search() {
  const [state, setState] = useState([]);
  const [query, setQuery] = useState('');
  const [error, setError] = useState('');

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(event.target.value);
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const req: RequestConfig = {
      api_key: API_KEY,
      q: query,
      limit: API_LIMIT
    }
    makeRequest(req).then(res => {
      setState(res.data);
      setError('');
    }).catch(error => {
      setError(`Error: ${error.response.data.message}`);
    });
  }

  const displayResult = (s: any) => {
    return s.map((e:any) => <li key={e.id}><img alt={e.title} src={e.images.fixed_height.url} /></li>)
  }

  return (
    <div className="container">
      <div className="search__description">
        Search gifs on Giphy
      </div>
      <div className="search__helper-text">
        GIPHY is a platform which allows websites and apps to integrate gifs into their system.
      </div>
      <div className="search__wrapper">
        <form className="search__form" onSubmit={handleSubmit}>
          <div className="search__form__element">
            <label htmlFor="search">Search</label>
            <input type="text" id="search" name="search" value={query} onChange={handleInputChange} />
          </div>
          {/* <div className="search__form__submit">
            <input type="submit" value="Search" />
          </div> */}
        </form>
        {
          error !== '' ?
            <div className="search__error">{ error }</div>
            :null
        }
      </div>
      <ul className="search__result">
        { displayResult(state) }
      </ul>
    </div>
  );
}

export default Search;
