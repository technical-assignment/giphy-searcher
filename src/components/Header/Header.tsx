import './Header.scss';

function Header() {
  return (
    <header className="header">
      <div className="container">
        <h1 className="header__title">GIPHY Searcher</h1>
      </div>
    </header>
  );
}

export default Header;
