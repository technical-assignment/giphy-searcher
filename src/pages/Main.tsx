import './Main.scss';
import Header from '../components/Header/Header';
import Search from '../components/Search/Search';

function Main() {
  return (
    <div className="Main">
      <Header />
      <Search />
    </div>
  );
}

export default Main;
