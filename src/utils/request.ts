import axios, { AxiosRequestConfig } from 'axios';
import RequestConfig from './request.config';
import { API_BASE_PATH } from '../constants/constants';

function buildUrl(requestConfig: RequestConfig): string {
  let requestUrl = `${API_BASE_PATH}?`;
  Object.entries(requestConfig).forEach(
    ([key, value]) => requestUrl += `&${key}=${value}`
  );
  return requestUrl;
}

async function makeRequest(requestConfig: RequestConfig):Promise<any> {
  const config: AxiosRequestConfig = {
    method: 'get',
    url: buildUrl(requestConfig),
  }
  
  let { data } = await axios(config);

  return data;
}

export default makeRequest;
