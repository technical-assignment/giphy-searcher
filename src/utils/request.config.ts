interface RequestConfig {
  api_key: String;
  q?: String;
  limit?: Number;
}

export default RequestConfig;
